networking-sfc (19.0.0-2) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090444).

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Dec 2024 13:49:49 +0100

networking-sfc (19.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 02 Oct 2024 16:47:48 +0200

networking-sfc (19.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sat, 21 Sep 2024 14:12:53 +0200

networking-sfc (19.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Requires SQLAlchemy >= 2.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Sep 2024 11:42:42 +0200

networking-sfc (18.0.0-3) unstable; urgency=medium

  * Restrict autopkgtest to Architecture: amd64 arm64 ppc64el.

 -- Thomas Goirand <zigo@debian.org>  Mon, 06 May 2024 16:53:10 +0200

networking-sfc (18.0.0-2) unstable; urgency=medium

  * Drop extraneous python3-mock build dependency (Closes: #1069976).

 -- Thomas Goirand <zigo@debian.org>  Mon, 29 Apr 2024 08:55:34 +0200

networking-sfc (18.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 07 Apr 2024 22:14:10 +0200

networking-sfc (18.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Replaced do-not-use-assertDictContainsSubset.patch by the nicer upstream
    patch py312-assertDictContainsSubset.patch, thanks to James Page for it.

 -- Thomas Goirand <zigo@debian.org>  Wed, 13 Mar 2024 12:19:09 +0100

networking-sfc (17.0.0-2) unstable; urgency=medium

  * Add do-not-use-assertDictContainsSubset.patch (Closes: #1058258).

 -- Thomas Goirand <zigo@debian.org>  Sun, 07 Jan 2024 11:15:35 +0100

networking-sfc (17.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 05 Oct 2023 12:57:12 +0200

networking-sfc (17.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Cleans better.

 -- Thomas Goirand <zigo@debian.org>  Fri, 15 Sep 2023 10:40:25 +0200

networking-sfc (16.0.0-3) unstable; urgency=medium

  * Cleans better (Closes: #1045774).

 -- Thomas Goirand <zigo@debian.org>  Tue, 15 Aug 2023 17:02:52 +0200

networking-sfc (16.0.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Tue, 20 Jun 2023 10:17:38 +0200

networking-sfc (16.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Mar 2023 16:59:06 +0100

networking-sfc (16.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Removed (build-)depends versions when satisfied in Bookworm.

 -- Thomas Goirand <zigo@debian.org>  Thu, 02 Mar 2023 10:27:30 +0100

networking-sfc (15.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 05 Oct 2022 23:43:22 +0200

networking-sfc (15.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed min version of python3-neutron (>= 2:21.0.0~).

 -- Thomas Goirand <zigo@debian.org>  Sun, 18 Sep 2022 13:09:11 +0200

networking-sfc (14.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 30 Mar 2022 21:53:10 +0200

networking-sfc (14.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 25 Mar 2022 15:28:55 +0100

networking-sfc (14.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Add autopkgtest.

 -- Thomas Goirand <zigo@debian.org>  Sat, 12 Mar 2022 18:56:43 +0100

networking-sfc (13.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 06 Oct 2021 21:20:37 +0200

networking-sfc (13.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Sep 2021 13:32:05 +0200

networking-sfc (13.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Needs python3-neutron-lib >= 2.11.0.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Sep 2021 10:42:42 +0200

networking-sfc (12.0.0-2) unstable; urgency=medium

  * d/control: Add me to uploaders field
  * d/copyright: Add me to copyright file
  * Upload to unstable

 -- Michal Arbet <michal.arbet@ultimum.io>  Mon, 16 Aug 2021 16:41:13 +0200

networking-sfc (12.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Apr 2021 16:45:56 +0200

networking-sfc (12.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Removed (build-)depends versions when satisfied in Bullseye.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 25 Mar 2021 12:14:47 +0100

networking-sfc (11.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.
  * Fixed debian/watch.
  * Add a debian/salsa-ci.yml.

 -- Thomas Goirand <zigo@debian.org>  Sun, 18 Oct 2020 15:01:40 +0200

networking-sfc (11.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sun, 27 Sep 2020 20:58:55 +0200

networking-sfc (10.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 13 May 2020 17:02:49 +0200

networking-sfc (10.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 10 May 2020 12:41:02 +0200

networking-sfc (10.0.0~rc1-1) experimental; urgency=medium

  * Move the package to the neutron-plugins subgroup on Salsa.
  * New upstream release.
  * Added python3-sphinxcontrib.svg2pdfconverter as build-depends.

 -- Thomas Goirand <zigo@debian.org>  Wed, 29 Apr 2020 23:41:29 +0200

networking-sfc (9.0.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Thomas Goirand ]
  * Uploading go unstable.

 -- Thomas Goirand <zigo@debian.org>  Tue, 22 Oct 2019 23:52:04 +0200

networking-sfc (9.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 16 Oct 2019 20:33:32 +0200

networking-sfc (9.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 03 Oct 2019 22:37:21 +0200

networking-sfc (8.0.0-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Thomas Goirand ]
  * Uploading to unstable.
  * Minor fixes in debian/control.

 -- Thomas Goirand <zigo@debian.org>  Sat, 20 Jul 2019 15:41:56 +0200

networking-sfc (8.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 11 Apr 2019 13:31:34 +0200

networking-sfc (8.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Removed package versions when satisfied in Buster.

 -- Thomas Goirand <zigo@debian.org>  Tue, 02 Apr 2019 14:49:09 +0200

networking-sfc (7.0.0-1) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 05 Sep 2018 16:52:00 +0200

networking-sfc (7.0.0~rc1-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Running wrap-and-sort -bast
  * d/control: Use team+openstack@tracker.debian.org as maintainer
  * d/control: Add trailing tilde to min version depend to allow
    backports

  [ Thomas Goirand ]
  * Fixed watch file pointing to the wrong project.
  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 28 Aug 2018 21:47:14 +0200

networking-sfc (6.0.0-1) unstable; urgency=medium

  [ Daniel Baumann ]
  * Updating copyright format url.
  * Updating maintainer field.
  * Running wrap-and-sort -bast.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Add trailing tilde to min version depend to allow
    backports

  [ Thomas Goirand ]
  * New upstream release:
    - Fixes FTBFS (Closes: #884563).
    - Works with SQLA 1.1 (Closes: #852800).
  * Fixed (build-)depends for this release.
  * Switched to Python 3.
  * Removed patches.
  * Uploading to unstable.
  * Standards-Version is now 4.1.3.
  * Reviewed debian/copyright holder list and years.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Mar 2018 22:36:37 +0000

networking-sfc (2.0.1~git20160926.27f311e-1) experimental; urgency=medium

  * Initial release (Closes: #833729).

 -- James Page <james.page@ubuntu.com>  Mon, 26 Sep 2016 11:09:09 +0100
